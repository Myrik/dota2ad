import httpx
from parsel import Selector

from schemas.abilitydb import AbilityDB
from schemas.combos import AbilityCombos


def load_combos(abilities: AbilityDB) -> AbilityCombos:
    ac = AbilityCombos()

    r = httpx.get("https://windrun.io/ability-pairs")
    data = Selector(text=r.text)
    tbody = data.xpath('//table[@id="ability-pair-stats"]/tbody/tr')

    for i in tbody:
        i: Selector
        (
            ability_1_img,
            ability_1_text,
            ability_1_wr,
            ability_2_img,
            ability_2_text,
            ability_2_wr,
            _,
            combo_wr,
            synergy,
        ) = i.xpath("./td")

        def get_slug(img):
            img = img.xpath("./img/@src").get()
            return img.split("/")[-1].split(".")[0]

        ability_1 = abilities.get_ability_by_slug(get_slug(ability_1_img))
        ability_2 = abilities.get_ability_by_slug(get_slug(ability_2_img))

        ac.add_combo(
            ability_1,
            ability_2,
            float(combo_wr.xpath("./text()").get().strip().rstrip("%")),
        )

    return ac


def main():
    ability_db = AbilityDB.load()
    ac = load_combos(ability_db)
    ac.save_combos()
    print()


if __name__ == "__main__":
    main()
