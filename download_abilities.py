import asyncio
import json
import re
from typing import Any, Dict

import httpx


def get_abils() -> Dict[str, Dict[str, Any]]:
    data = httpx.get(
        "https://raw.githubusercontent.com/odota/dotaconstants/master/build/abilities.json"
    ).json()

    results = {}

    for slug, v in data.items():
        behavior = v.get("behavior") or []
        if isinstance(behavior, str):
            behavior = [behavior]

        name = v.get("dname")

        if not name:
            continue

        results[slug] = {"slug": slug, "name": name, "hidden": "Hidden" in behavior}

    return results


async def get_hero_abils(abils: Dict[str, Dict[str, Any]]):
    data = httpx.get(
        "https://raw.githubusercontent.com/odota/dotaconstants/master/build/hero_abilities.json"
    ).json()

    result = {}

    for hero, stats in sorted(data.items(), key=lambda x: x[0]):
        hero_name = hero.replace("npc_dota_hero_", "")

        abilities = stats["abilities"]

        r = [abils.get(i) for i in abilities]
        u = False
        for i in r[::-1]:
            if i and not i["hidden"] and not u:
                i["ultimate"] = True
                u = True
            elif i:
                i["ultimate"] = False

        hero_abils = [i for i in r if i and not re.findall(r"empty\d$", i["slug"])]
        result[hero_name] = hero_abils

        await asyncio.gather(*(download_ability_image(i["slug"]) for i in hero_abils))
        # for i in hero_abils:
        #     print(f"Download ability: {i['name']}")
        #     download_ability_image(i["slug"])

    return result


async def download_ability_image(slug: str):
    async with httpx.AsyncClient() as c:
        print(f"Download ability: {slug}")
        r = await c.get(
            f"https://cdn.datdota.com/images/ability/{slug}.png", timeout=30
        )
        if r.is_success:
            with open(f"images/{slug}.png", "wb") as f:
                f.write(r.content)


async def main():
    abils = get_abils()
    hero_abils = await get_hero_abils(abils)

    with open("/home/myrik/work/my/dota2ad/abilities.json", "w") as f:
        json.dump(hero_abils, f, indent=2)


if __name__ == "__main__":
    asyncio.run(main())
