import asyncio
import logging
from concurrent.futures import ProcessPoolExecutor
from datetime import datetime
from typing import List, Tuple

from PIL import Image

from parse_image.parse_abils import get_abilities_block_image, get_ability_images
from parse_image.parse_ults import get_ultimate_images, get_ultimates_image
from schemas.abilitydb import Ability, AbilityDB
from schemas.combos import AbilityCombos
from utils.notifications import notification
from utils.screenshoter import get_dota2_image

ULT_IMAGES_TYPE = tuple[
    Image.Image, Image.Image, Image.Image, Image.Image, Image.Image, Image.Image
]

log = logging.getLogger()
logging.basicConfig(level=logging.INFO)


async def get_abilities(
    images: tuple[tuple[Image.Image, ...], ...], ability_db: AbilityDB, ult: bool
) -> list[list[Ability, ...], ...]:
    result = []
    loop = asyncio.get_running_loop()

    tasks = []

    with ProcessPoolExecutor() as executor:
        for line in images:
            tasks_line = []
            tasks.append(tasks_line)

            result_line = []
            result.append(result_line)

            for i in line:
                task = loop.run_in_executor(
                    executor, ability_db.get_ability_by_img, i, ult
                )
                tasks_line.append(task)

                # ability = abilities.get_ability_by_img(i, ult=ult)
                # result_line.append(ability)

        for t, r in zip(tasks, result):
            for j in t:
                r.append(await j)

    return result


async def parse_abilities(
    ability_db: AbilityDB, img: Image.Image
) -> Tuple[List[Ability], List[Ability]]:
    log.info("Find squares")

    abilities_image = get_abilities_block_image(img)
    ultimates_image = get_ultimates_image(img)

    # abilities_image.show()
    # ultimates_image.show()

    log.info("Get images")
    ability_images = get_ability_images(abilities_image)
    ultimate_images = get_ultimate_images(ultimates_image)

    log.info("Find abilities")

    t = datetime.now()
    abilities, ultimate_abilities = await asyncio.gather(
        get_abilities(ability_images, ability_db, False),
        get_abilities(ultimate_images, ability_db, True),
    )
    print(datetime.now() - t)
    # abilities = get_abilities(ability_images, cc, False)
    # ultimate_abilities = get_abilities(ultimate_images, cc, True)

    return abilities, ultimate_abilities


async def get_combos(
    ability_db: AbilityDB, abilities: List[Ability], ultimate_abilities: List[Ability]
) -> list[tuple[Ability, Ability], float]:
    ac = AbilityCombos.load(ability_db)
    log.info("Find combos")
    combo_list = ac.get_combos_list(abilities, ultimate_abilities)

    return combo_list


async def main():
    # img = Image.open("screenshot/2.png").convert("RGB")
    img = get_dota2_image()
    log.info("Load abilities")
    ability_db = AbilityDB.load()
    abilities, ultimate_abilities = await parse_abilities(ability_db, img)
    combo_list = await get_combos(ability_db, abilities, ultimate_abilities)

    lines = []
    for (a1, a2), wr in combo_list[:5]:
        lines.append(f"{a1.name} + {a2.name}: {wr}%")

    text = "\n".join(lines)
    print(text)
    notification(text)


if __name__ == "__main__":
    asyncio.run(main())
