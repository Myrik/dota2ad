from PIL import Image
from PIL.Image import Resampling, Transform

from parse_image.find_coeffs import find_coeffs


def get_abilities_block_image(img):
    width, height = img.size
    box = (
        int(width / 2.88),
        int(height / 3.2),
        int(width - width / 2.88 + 2),
        int(height - height / 4.5),
    )

    img = img.crop(box)
    # img.show()

    width, height = img.size
    source_points = [(0, 0), (width, 0), (width, height), (0, height)]
    destination_points = [
        (width / 11, 0),
        (width - (width / 11), 0),
        (width, height),
        (0, height),
    ]

    coefs = find_coeffs(
        source_points,
        destination_points,
    )

    img = img.transform(
        (width, height),
        Transform.PERSPECTIVE,
        coefs,
        Resampling.BICUBIC,
    )

    return img.resize((500, 500))


def get_ability_images(img):
    # img.show()
    width, height = img.size

    upper_space = height / 50
    bottom_space = height / 26
    side_space = width / 48
    both_lines_box = (
        int(side_space) + 5,
        int(upper_space),
        int(width - side_space),
        int(height - bottom_space),
    )

    cropped = img.crop(both_lines_box)
    # cropped.show()

    width, height = cropped.size

    # image_size = width / 10
    block_size = width / 2.3

    block_boxes = [
        (0, 0, block_size, block_size - 5),
        (width - block_size - 5, 0, width, block_size - 5),
        (0, height - block_size + 5, block_size, height),
        (width - block_size - 5, height - block_size + 5, width, height),
    ]
    blocks = list(map(lambda x: cropped.crop(x).resize((200, 200)), block_boxes))
    block_images = list(map(parse_abilities_block, blocks))
    image_matrix: list[list[Image.Image]] = [
        [*block_images[0][:3], *block_images[1][:3]],
        [*block_images[0][3:6], *block_images[1][3:6]],
        [*block_images[0][6:9], *block_images[1][6:9]],
        [*block_images[2][:3], *block_images[3][:3]],
        [*block_images[2][3:6], *block_images[3][3:6]],
        [*block_images[2][6:9], *block_images[3][6:9]],
    ]

    return image_matrix


def parse_abilities_block(img: Image.Image):
    width, height = img.size

    center_width = width / 2
    center_height = height / 2

    img_size = width / 4.1

    boxes = [
        (0, 0, img_size, img_size),
        (center_width - img_size / 2, 0, center_width + img_size / 2, img_size),
        (width - img_size, 0, width, img_size),
        (0, center_height - img_size / 2, img_size, center_height + img_size / 2),
        (
            center_width - img_size / 2,
            center_height - img_size / 2,
            center_width + img_size / 2,
            center_height + img_size / 2,
        ),
        (
            width - img_size,
            center_height - img_size / 2,
            width,
            center_height + img_size / 2,
        ),
        (0, height - img_size, img_size, height),
        (
            center_width - img_size / 2,
            height - img_size,
            center_width + img_size / 2,
            height,
        ),
        (width - img_size, height - img_size, width, height),
    ]

    images = list(map(lambda x: img.crop(x).resize((64, 64)), boxes))

    return images
