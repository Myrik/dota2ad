from PIL import Image
from PIL.Image import Resampling, Transform

from parse_image.find_coeffs import find_coeffs

ULT_IMAGES_TYPE = tuple[
    Image.Image, Image.Image, Image.Image, Image.Image, Image.Image, Image.Image
]


def get_ultimates_image(img):
    width, height = img.size
    box = (
        int(width / 2.84),
        int(height / 7.66),
        int(width - width / 2.84),
        int(height / 7.66 + height / 5.4),
    )

    img = img.crop(box)
    # img.show()

    width, height = img.size
    source_points = [(0, 0), (width, 0), (width, height), (0, height)]
    destination_points = [
        (0, 0),
        (width, 0),
        (width - width / 45, height),
        (width / 45, height),
    ]

    coefs = find_coeffs(
        source_points,
        destination_points,
    )

    img = img.transform(
        (width, height),
        Transform.PERSPECTIVE,
        coefs,
        Resampling.BICUBIC,
    )

    return img


def get_line_ult_images(line: Image.Image) -> ULT_IMAGES_TYPE:
    width, ability_heigth = line.size
    ability_space = width / 13

    images = []

    for i in range(6):
        space = int((ability_space + ability_heigth) * i)
        ability_img = line.crop(
            (space, 0, ability_heigth + space, ability_heigth)
        ).resize((64, 64))
        images.append(ability_img)

    return images[0], images[1], images[2], images[3], images[4], images[5]


def get_ultimate_images(img: Image.Image) -> tuple[ULT_IMAGES_TYPE, ULT_IMAGES_TYPE]:
    width, height = img.size

    upper_space = height / 8
    bottom_space = height / 10
    side_space = width / 60
    both_lines_box = (
        int(side_space) + 3,
        int(upper_space),
        int(width - side_space),
        int(height - bottom_space),
    )

    both_lines = img.crop(both_lines_box)
    # both_lines.show()
    width, height = both_lines.size
    ability_heigth = int(height / 2.73)

    line1_box = (0, 0, width, ability_heigth)
    line2_box = (0, int(height - ability_heigth), width, height)

    line1 = both_lines.crop(line1_box)
    # line1.show()
    line2 = both_lines.crop(line2_box)
    # line2.show()

    # line1.save("line1.png")
    # line2.save("line2.png")

    # ability_space = width / 13.2
    ability_images = (get_line_ult_images(line1), get_line_ult_images(line2))

    # ability_images[-1].show()

    # ability11.show()
    # ability12 = line1.crop(
    #     (ability_space, 0, ability_heigth + ability_space, ability_heigth)
    # )
    # ability12.show()

    return ability_images
