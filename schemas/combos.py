import json
from typing import List, Optional

from pydantic import BaseModel, Field

from schemas.ability import Ability
from schemas.abilitydb import AbilityDB


class AbilityCombos(BaseModel):
    combos_wr: dict[tuple[Ability, Ability], float] = Field(default_factory=dict)

    def add_combo(self, a1: Ability, a2: Ability, wr: float):
        self.combos_wr[(a1, a2)] = wr

    def get_wr(self, a1: Ability, a2: Ability) -> Optional[float]:
        return self.combos_wr.get((a1, a2)) or self.combos_wr.get((a1, a2))

    def save_combos(self, filename="combos.json"):
        combos = [
            {
                "a1": a1.slug,
                "a2": a2.slug,
                "wr": v,
            }
            for (a1, a2), v in sorted(
                self.combos_wr.items(), key=lambda x: (x[0][0].slug, x[0][1].slug)
            )
        ]
        with open(filename, "w") as f:
            json.dump(combos, f, indent=2)

    def get_combos_list(
        self, abilities: List[List[Ability]], ultimate_abilities: List[List[Ability]]
    ):
        all_abilities = []

        for line in ultimate_abilities:
            all_abilities += line
        for line in abilities:
            all_abilities += line

        abilities_set = set(all_abilities)

        combos = []

        for (a1, a2), wr in self.combos_wr.items():
            if a1 in abilities_set and a2 in abilities_set and a1.hero != a2.hero:
                combos.append(((a1, a2), wr))

        combos = sorted(combos, key=lambda x: x[1], reverse=True)

        return combos

    @classmethod
    def load(cls, ability_db: AbilityDB, filename="combos.json"):
        with open(filename) as f:
            data = json.load(f)

        obj = cls()

        for c in data:
            a1 = ability_db.get_ability_by_slug(c["a1"])
            a2 = ability_db.get_ability_by_slug(c["a2"])
            wr = c["wr"]
            obj.add_combo(a1, a2, wr)

        return obj
