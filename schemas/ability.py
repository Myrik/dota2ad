from typing import Optional

from PIL import Image
from pydantic import BaseModel, Field

_images_cache = {}


class Ability(BaseModel):
    class Config:
        frozen = True

    slug: str = Field(..., repr=False)
    name: str
    ultimate: bool = Field(..., repr=False)
    hidden: bool = Field(..., repr=False)
    winrate: Optional[float] = Field(None, repr=False)
    hero: str

    @property
    def image(self) -> Image.Image:
        if self in _images_cache:
            return _images_cache[self]

        try:
            img = Image.open(f"images/{self.slug}.png")
            result = img.resize((64, 64)).convert("RGB")
        except:
            result = None

        _images_cache[self] = result

        return result
