import json

from PIL import ImageOps
from pydantic import BaseModel
from skimage import img_as_float
from skimage.metrics import mean_squared_error

from schemas.ability import Ability


class AbilityDB(BaseModel):
    abilities: list[Ability]

    @classmethod
    def load(cls, filename="abilities.json"):
        with open(filename) as f:
            data = json.load(f)

        abilities = []
        for hero, hero_abilities in data.items():
            for hero_ability in hero_abilities:
                abilities.append(Ability(**hero_ability, hero=hero))

        obj = cls(abilities=sorted(set(abilities), key=lambda x: x.name))

        return obj

    def get_ability_by_img(self, img, ult=None):
        results = []

        # 0.0001361580682109445
        gray_err = mean_squared_error(
            img_as_float(ImageOps.grayscale(img).convert("RGB")), img_as_float(img)
        )
        if gray_err < 0.0002:
            return

        for i in self.abilities:
            if i.hidden:
                continue
            if ult is not None and ult != i.ultimate:
                continue

            ability_img = i.image

            if not ability_img:
                continue

            err = mean_squared_error(img_as_float(img), img_as_float(ability_img))
            results.append((round(err, 3), i))

        rez = sorted(results, key=lambda x: x[0])

        err, ability = rez[0]

        if err < 0.08:
            # print(ability)
            return ability

    def get_ability_by_slug(self, slug: str):
        for i in self.abilities:
            if i.slug == slug:
                return i
