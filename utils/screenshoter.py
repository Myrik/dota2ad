import re
import subprocess
from io import BytesIO

from PIL import Image
from wand.image import Image as wImage


def get_xwd(window_id: int) -> bytes:
    p = subprocess.Popen(
        f"xwd -id {hex(window_id)} -silent",
        shell=True,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
    )
    data = p.communicate()

    if data[0]:
        return data[0]


def find_window_id(name: str) -> int:
    p = subprocess.Popen(
        f'xwininfo -name "{name}"',
        shell=True,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
    )
    text = p.communicate()[0]
    window_id = re.findall(r"Window id: 0x([\da-f]+)", text.decode())
    if window_id:
        return int(window_id[0], 16)


def get_dota2_image() -> Image.Image:
    window_id = find_window_id("Dota 2")
    data = get_xwd(window_id)
    with wImage(blob=data, format="xwd") as im:
        result = im.make_blob("jpeg")
    return Image.open(BytesIO(result))
