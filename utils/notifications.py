import notify2


app_name = "dota2picker"

notify2.init(app_name)


def notification(message):
    notice = notify2.Notification(app_name, str(message), "dota2")
    notice.show()
